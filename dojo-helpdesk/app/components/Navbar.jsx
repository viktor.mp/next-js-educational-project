import Link from 'next/link'
import React from 'react'
import LogOutButton from './LogOutButton'

export default function Navbar({user}) {
  return (
    <nav>
          <h1>Dojo Helpdesk</h1>
          <Link href='/'>Dashboard</Link>
          <Link href='/tickets' className='mr-auto'>Tickets</Link>

          {user && <span>Hello {user.email}</span>}
          <LogOutButton />
    </nav>
  )
}
