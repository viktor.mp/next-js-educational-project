import React from 'react'

export default function verify() {
  return (
    <main className="text-center">
        <h2>Thank for registering</h2>
        <p>Before logging in, you need to verify your email address.</p>
    </main>
  )
}
