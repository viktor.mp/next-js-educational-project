import CreateForm from "./CreateFrom";

export default function CreateTicket() {
  return (
    <main>
        <h2 className="text-primary text-center">Add a new ticket</h2>
        <CreateForm />
    </main>
  )
}
